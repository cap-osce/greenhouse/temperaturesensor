# TemperatureSensor

ESP32 code to read the temperature value from an onewire temperature sensor (DS18S20)

The Vcc pin for the sensor needs to be connected to the Vcc of the board (3V do not
  connect it to a 5V pin since the ESP32 is 3v based)

The data pin from the temperature sensor should be connected to pin 32 (can be changed
  in this line of code OneWire  ds(32); in the main application)

Between the Vcc and the Data pin, you need to place a 4.7K resistor (likely a pull-up) 
Hello
