#include <Arduino.h>
#include <Wire.h>  
#include "SSD1306Wire.h" 
#include <OneWire.h>
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include "Sensor.h"
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <DallasTemperature.h>


// Initialize the OLED display using Wire library
SSD1306Wire  display(0x3c, 5, 4);
OneWire  ds(32);
DallasTemperature temperature(&ds);
DeviceAddress probe;

IOTGateway iotGateway(&capgeminiHackathon);
Sensor sensor("temperature", "temperature", 5000, 5000);

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 0, 60000);

// Variables to save date and time
String formattedDate;
String dayStamp;
String timeStamp;

struct TemperatureData
{
     float celsuis;
     float fahrenheit;
};

void setup() {
  Serial.begin(115200);
  
  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();
  display.clear();
  Serial.println("Setup complete");

  iotGateway.initialise();
  delay(2500);
  timeClient.begin();
  timeClient.update();
  temperature.begin();
  if (!temperature.getAddress(probe, 0)) {
    Serial.println("Failed to get probe address");
  }
}

void displayTemperature(float celsius) {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 0, "Temperature");
  display.setFont(ArialMT_Plain_24);

  char str_temp[6];

  /* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
  dtostrf(celsius, 4, 2, str_temp);
  char temperature[7];
  sprintf(temperature,"%s c", str_temp);
  display.drawString(30, 20, temperature);
  display.display();
}

char msg[512];
char scrn_msg[5];
char ts_txt[16];
long lastTime = 0;
TemperatureData temperatureData;

void calculateTemperature() {
  float celsius, fahrenheit;
  temperature.requestTemperatures();
  celsius = temperature.getTempC(probe);
  fahrenheit = temperature.toFahrenheit(celsius);
  temperatureData = { celsius, fahrenheit };
}

void publishTemperature() {
    char fullTimestamp [25];
    sprintf (fullTimestamp, "%ld", timeClient.getEpochTime());
    lastTime = millis();
    sensor.formatData(msg, 512, temperatureData.celsuis, "temperature", fullTimestamp);
    iotGateway.publish("measurement/temperature", msg);
}

void loop() {
  calculateTemperature();

  if (temperatureData.celsuis > -100.0) {
    if (millis() - lastTime > 30000) {
      Serial.println();
      Serial.println();
      publishTemperature();
      Serial.println();
      Serial.print("  Temperature = ");
      Serial.print(temperatureData.celsuis);
      Serial.print(" Celsius, ");
      Serial.print(temperatureData.fahrenheit);
      Serial.println(" Fahrenheit");
    }

    displayTemperature(temperatureData.celsuis);
  }
}